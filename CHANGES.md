# Changes for the "CodeCity: A Comparison of On-Screen and Virtual Reality" paper

## Questionnaire and Tasks

- Added training section
- Supervisor must note the metric value of each task in order to measure the deviation between the correct answers and the answer provided.
- Added a new task (T4 and T5.4) that asks about the volume (lines of code) of the buildings.
- Added a new task that is more complex (T6 and T5.6), we ask for an specific building that is in an specific folder. This task has the aim of prove whether vr or screen is more efficient (quicker) solving this type of complex tasks. Moreover, the building is located in the background of the city, so que user needs to move a little to find it.

## Scenes

- Added a new scene called training, with non-relevant data but a similar environment than the real experiment scenes.
- Increased quarter heights to improve VR experience
- Decreased city size to improve VR experience
- Added the new two tasks
- Environment for VR is the same for Screen, the tasks are there and the links as well.