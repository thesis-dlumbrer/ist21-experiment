# CodeCity: A Comparison of On-Screen and Virtual Reality - Replication Package

## Scenes

[Go here to see the scenes](https://thesis-dlumbrer.gitlab.io/ist21-experiment)

### Deploy them locally

The easiest way to do it is to go to the `scenes` folder and deploy the `scenes/index.html` file within a simple HTTP server (i.e. node http-server or python simple http server).

(Optional) Examples of http servers deploy:
```shell
# Using node [1]
$> npm install -g http-server
$> http-server

# Using python 2 [2]
$> python -m SimpleHTTPServer

# Using python 3 [3]
$> python -m http.server
```

### Specific scenes

These are the HTML files with the specific scenes for each type of participant (VR and On-Screen), the replicant should be able to visit them through the links of the `scenes/index.html` page (visit [Deploy](#Deploy) section):

- `scenes/screen/jetuml2021.html`: Scene for the On-Screen participants for the JetUML version as of March 24, 2021
- `scenes/screen/training.html`: Scene for the On-Screen participants for the training with the A-Frame scene. The data showed is not relevant.
- `scenes/vr/jetuml2021.html`: Scene for the VR participants for the JetUML version as of March 24, 2021
- `scenes/screen/training.html`: Scene for the VR participants for the the training with the A-Frame scene. The data showed is not relevant.

You can deploy each html isolated using a simple http server on each path.

## Participants questionnaire

Inside the `questionnaire` folder there are in different file format, the questionnaire presented to the participants:

- [Markdown version](./questionnaire/questionnaire.md)
- [PDF version](./questionnaire/questionnaire.pdf)
- [HTML version](./questionnaire/questionnaire.html)
- [Supervisor notes for relaizing the experiment](./questionnaire/supervisor_notes.md)

### Columns description

- **T1_location**: Answer to the question of Task 1 "Locate all the test code (file and directory) of the system and identify their position in the city. Where is the location of the files/directories?. Select them and say where they are."
- **T2_file1**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the first file with the highest number of functions**
- **T2_file2**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the second file with the highest number of functions**
- **T2_file3**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the third file with the highest number of functions**
- **T2_file4**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the fourth file with the highest number of functions**
- **T2_file5**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the fifth file with the highest number of functions**
- **T3_file1**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the first file with the highest lines of code per function**
- **T3_file2**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the second file with the highest lines of code per function**
- **T3_file3**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the third file with the highest lines of code per function**
- **T3_file4**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the fourth file with the highest lines of code per function**
- **T3_file5**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the fifth file with the highest lines of code per function**
- **T4_file1**: Answer to the question of Task 4 "The base/area of the buildings represents the number of functions in the code file (num_funs), the height represents the lines of code per function in the code file. Therefore, the volume (base x height) corresponds to the total number of lines of code. Find the three source code files (not testing files) with the highest number of lines of code in the system. Say their names in order." **This is for the first file with the highest lines of code**
- **T4_file2**: Answer to the question of Task 4 "The base/area of the buildings represents the number of functions in the code file (num_funs), the height represents the lines of code per function in the code file. Therefore, the volume (base x height) corresponds to the total number of lines of code. Find the three source code files (not testing files) with the highest number of lines of code in the system. Say their names in order." **This is for the second file with the highest lines of code**
- **T4_file3**: Answer to the question of Task 4 "The base/area of the buildings represents the number of functions in the code file (num_funs), the height represents the lines of code per function in the code file. Therefore, the volume (base x height) corresponds to the total number of lines of code. Find the three source code files (not testing files) with the highest number of lines of code in the system. Say their names in order." **This is for the third file with the highest lines of code**
- **T4_file4**: Answer to the question of Task 4 "The base/area of the buildings represents the number of functions in the code file (num_funs), the height represents the lines of code per function in the code file. Therefore, the volume (base x height) corresponds to the total number of lines of code. Find the three source code files (not testing files) with the highest number of lines of code in the system. Say their names in order." **This is for the fourth file with the highest lines of code**
- **T4_file5**: Answer to the question of Task 4 "The base/area of the buildings represents the number of functions in the code file (num_funs), the height represents the lines of code per function in the code file. Therefore, the volume (base x height) corresponds to the total number of lines of code. Find the three source code files (not testing files) with the highest number of lines of code in the system. Say their names in order." **This is for the fifth file with the highest lines of code**
- **T5_file1**: Answer to the question of Task 5 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the first file with the highest ccn**
- **T5_file2**: Answer to the question of Task 5 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the second file with the highest ccn**
- **T5_file3**: Answer to the question of Task 5 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the third file with the highest ccn**
- **T5_file4**: Answer to the question of Task 5 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the fourth file with the highest ccn**
- **T5_file5**: Answer to the question of Task 5 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the fifth file with the highest ccn**
- **T6_file1**: Answer to the question of Task 6 "Find the source code file (not testing files) with the highest lines of code per function (highest loc_per_function) inside the src/ca/mcgill/cs/jetuml/diagram/nodes folder. Say their names in order." **This is for the first file with the highest loc per function in the directory**
- **T6_file2**: Answer to the question of Task 6 "Find the source code file (not testing files) with the highest lines of code per function (highest loc_per_function) inside the src/ca/mcgill/cs/jetuml/diagram/nodes folder. Say their names in order." **This is for the second file with the highest loc per function in the directory**
- **T6_file3**: Answer to the question of Task 6 "Find the source code file (not testing files) with the highest lines of code per function (highest loc_per_function) inside the src/ca/mcgill/cs/jetuml/diagram/nodes folder. Say their names in order." **This is for the third file with the highest loc per function in the directory**

> IMPORTANT: There is a column called `METRIC VALUE` with the numeric value of the metric represented in the geometry of the building.

## Results data

In the `results` folder there are the CSV files of the answers (raw and formatted) of the participants:

- `results/onscreen_formatted.csv`: Contains the formatted answers of the screen participants. [Click here to go to the file](./results/onscreen_formatted.csv)
- `results/vr_formatted.csv`: Contains the formatted answers of the VR participants. [Click here to go to the file](./results/vr_formatted.csv)

### Columns description

- **AGE**: Answer to the question of the Demographics survey "Age (for statistical purposes only)"
- **JOB_POSTION**: Answer to the question of the Demographics survey "Job position (i.e, developer, researcher, project manager, etc.)"
- **EXP_PRG**: Answer to the question of the Demographics survey "Experience level in (None, Beginner, Knowledgeable, Advanced, Expert): Programming"
- **EXP_IDE**: Answer to the question of the Demographics survey "Experience level in (None, Beginner, Knowledgeable, Advanced, Expert): Using a programming IDE (Pycharm, Eclipse, Visual Studio)"
- **YEARS_PRG**: Answer to the question of the Demographics survey "Number of years of (Less than 1, 1-3, 4-6, 7-10, 10+): Programming"
- **YEARS__IDE**: Answer to the question of the Demographics survey "Number of years of (Less than 1, 1-3, 4-6, 7-10, 10+): Using a programming IDE (Pycharm, Eclipse, Visual Studio)"
- **FAM_JETUML**: Answer to the question of the Demographics survey "Are you familiar with JetUML?"
- **EXP_SOFTVIS**: Answer to the question of the Demographics survey "Experience level in (None, Beginner, Knowledgeable, Advanced, Expert): Software visualization"
- **VR_HEADSET**: Answer to the question of the Demographics survey "Have you ever used a Virtual Reality headset (e.g., Oculus)? (None, Beginner, Knowledgeable, Advanced, Expert)"
- **T1_location**: Answer to the question of ask 1 "Locate all the test code (file and directory) of the system and identify their position in the city. Where is the location of the files/directories?. Select them and say where they are."
- **T1_dif**: Answer to the question of the Task 1 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T1_dt**: Total time spent completing Task 1
- **T2_file1**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the first file with the highest number of functions**
- **T2_file2**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the second file with the highest number of functions**
- **T2_file3**: Answer to the question of Task 2 "Find the three source code files (not testing files) with the highest number of functions (num_funs) in the system.". **This is for the third file with the highest number of functions**
- **T2_dif**: Answer to the question of Task 2 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T2_dt**: Total time spent completing Task 2
- **T3_file1**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the first file with the highest lines of code per function**
- **T3_file2**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the second file with the highest lines of code per function**
- **T3_file3**: Answer to the question of Task 3 "Find the three source code files (not testing files) with the highest lines of code per function (loc_per_function) in the system. Say their names in order." **This is for the third file with the highest lines of code per function**
- **T3_dif**: Answer to the question of Task 3 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T3_dt**: Total time spent completing Task 3
- **T4_file1**: Answer to the question of Task 4 "The base/area of the buildings represents the number of functions in the code file (num_funs), the height represents the lines of code per function in the code file. Therefore, the volume (base x height) corresponds to the total number of lines of code. Find the three source code files (not testing files) with the highest number of lines of code in the system. Say their names in order." **This is for the first file with the highest lines of code**
- **T4_file2**: Answer to the question of Task 4 "The base/area of the buildings represents the number of functions in the code file (num_funs), the height represents the lines of code per function in the code file. Therefore, the volume (base x height) corresponds to the total number of lines of code. Find the three source code files (not testing files) with the highest number of lines of code in the system. Say their names in order." **This is for the second file with the highest lines of code**
- **T4_file3**: Answer to the question of Task 4 "The base/area of the buildings represents the number of functions in the code file (num_funs), the height represents the lines of code per function in the code file. Therefore, the volume (base x height) corresponds to the total number of lines of code. Find the three source code files (not testing files) with the highest number of lines of code in the system. Say their names in order." **This is for the third file with the highest lines of code**
- **T4_dif**: Answer to the question of Task 4 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T4_dt**: Total time spent completing Task 4
- **T5_file1**: Answer to the question of Task 5 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the first file with the highest ccn**
- **T5_file2**: Answer to the question of Task 5 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the second file with the highest ccn**
- **T5_file3**: Answer to the question of Task 5 "Find the three source code files (not testing files) with the highest cyclomatic complexity number (highest ccn). Say their names in order." **This is for the third file with the highest ccn**
- **T5_dif**: Answer to the question of Task 5 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T5_dt**: Total time spent completing Task 5
- **T6_file1**: Answer to the question of Task 6 "Find the source code file (not testing files) with the highest lines of code per function (highest loc_per_function) inside the src/ca/mcgill/cs/jetuml/diagram/nodes folder. Say their names in order." **This is for the first file with the highest loc per function in the directory**
- **T6_dif**: Answer to the question of Task 6 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T6_dt**: Total time spent completing Task 6
- **T7_core**: Answer to the question of the Final Survey "After your experience with the cities and the metrics that it shows, identify what represents for you the core of the JetUML project. Select the location and say where it is."
- **T7_dif**: Answer to the question of Task 7 "Finding them was difficult. 'strongly agree, agree,  don’t know, disagree, strongly disagree'. Say your choice."
- **T7_dt**: Total time spent completing Task 7
- **DIFFICULT**: Answer to the question of the Final Survey "Overall, did you find the experiment difficult? 'strongly agree, agree,  don’t know, disagree, strongly disagree'"
- **SUGGESTION**: Answer to the question of the Final Survey "Any other suggestion/comment?"
- **SUPERVISOR NOTE**: If the supervisor noted something about the run.


### Results analysis

The analysis presented in the article, and the figures that are there were generated using a Jupyter notebook included in the `analysis` folder (`analysis.ipynb`), you can see the result of the analysis in the following formats:

- [Python notebook](./analysis/analysis.ipynb): `./analysis/analysis.ipynb` (if you want to execute it, you need to install in a python3 environment `jupyter-lab`, `pandas`, `matplotlib` and `scipy`)
- [Markdown](./analysis/analysis.md): `./analysis/analysis.md`
- [PDF](./analysis/analysis.pdf): `./analysis/analysis.pdf`
- [HTML](./analysis/analysis.html): `./analysis/analysis.html`

All the figures and tables have been used in the article.