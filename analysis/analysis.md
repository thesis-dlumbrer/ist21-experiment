# Analysis of the experiment results

This notebook contains all the analysis made to the set of results for the paper *CodeCity: On-Screen or in Virtual Reality?*


```python
import pandas as pd
import matplotlib.pyplot as plt

vr_results = pd.read_csv("../results/vr_formatted.csv")
screen_results = pd.read_csv("../results/onscreen_formatted.csv")
```

--------

## Subject data

In this section we analyze the data of the demographics survey, specifically, the experience of the participants


```python
# Experience level

# VR

df_to_create = vr_results

vr_explvl_df = pd.DataFrame({
    "None":[(df_to_create['EXP_PRG'] == 'None').sum(), (df_to_create['EXP_IDE'] == 'None').sum(), (df_to_create['EXP_SOFTVIS'] == 'None').sum(), (df_to_create['VR_HEADSET'] == 'None').sum()],
    "Beginner":[(df_to_create['EXP_PRG'] == 'Beginner').sum(), (df_to_create['EXP_IDE'] == 'Beginner').sum(), (df_to_create['EXP_SOFTVIS'] == 'Beginner').sum(), (df_to_create['VR_HEADSET'] == 'Beginner').sum()],
    "Knowledgeable":[(df_to_create['EXP_PRG'] == 'Knowledgeable').sum(), (df_to_create['EXP_IDE'] == 'Knowledgeable').sum(), (df_to_create['EXP_SOFTVIS'] == 'Knowledgeable').sum(), (df_to_create['VR_HEADSET'] == 'Knowledgeable').sum()],
    "Advanced":[(df_to_create['EXP_PRG'] == 'Advanced').sum(), (df_to_create['EXP_IDE'] == 'Advanced').sum(), (df_to_create['EXP_SOFTVIS'] == 'Advanced').sum(), (df_to_create['VR_HEADSET'] == 'Advanced').sum()],
    "Expert":[(df_to_create['EXP_PRG'] == 'Expert').sum(), (df_to_create['EXP_IDE'] == 'Expert').sum(), (df_to_create['EXP_SOFTVIS'] == 'Expert').sum(), (df_to_create['VR_HEADSET'] == 'Expert').sum()]
    }, 
    index=["Exp PRG", "Exp IDE", "Exp SoftVis", "Exp VR"]
)

# Screen

df_to_create = screen_results

screen_explvl_df = pd.DataFrame({
    "None":[(df_to_create['EXP_PRG'] == 'None').sum(), (df_to_create['EXP_IDE'] == 'None').sum(), (df_to_create['EXP_SOFTVIS'] == 'None').sum()],
    "Beginner":[(df_to_create['EXP_PRG'] == 'Beginner').sum(), (df_to_create['EXP_IDE'] == 'Beginner').sum(), (df_to_create['EXP_SOFTVIS'] == 'Beginner').sum()],
    "Knowledgeable":[(df_to_create['EXP_PRG'] == 'Knowledgeable').sum(), (df_to_create['EXP_IDE'] == 'Knowledgeable').sum(), (df_to_create['EXP_SOFTVIS'] == 'Knowledgeable').sum()],
    "Advanced":[(df_to_create['EXP_PRG'] == 'Advanced').sum(), (df_to_create['EXP_IDE'] == 'Advanced').sum(), (df_to_create['EXP_SOFTVIS'] == 'Advanced').sum()],
    "Expert":[(df_to_create['EXP_PRG'] == 'Expert').sum(), (df_to_create['EXP_IDE'] == 'Expert').sum(), (df_to_create['EXP_SOFTVIS'] == 'Expert').sum()]
    }, 
    index=["Exp PRG", "Exp IDE", "Exp SoftVis"]
)

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10, 4.5))
vr_explvl_df.plot(ax=axes[0], kind="bar", rot=0)
axes[0].legend(ncol=5,loc = 'upper right')
axes[0].set_ylabel("VR participant")
screen_explvl_df.plot(ax=axes[1], kind="bar", rot=0)
axes[1].legend(ncol=5,loc = 'upper right')
axes[1].set_ylabel("Screen participant")

# Uncomment to save fig
# plt.savefig('explvl2.png')

```




    Text(0, 0.5, 'Screen participant')




    
![png](output_3_1.png)
    



```python
# Experience Years

# VR

df_to_create = vr_results

vr_explvl_df = pd.DataFrame({
    "<1":[(df_to_create['YEARS_PRG'] == '<1').sum(), (df_to_create['YEARS_IDE'] == '<1').sum()],
    "1-3":[(df_to_create['YEARS_PRG'] == '1-3').sum(), (df_to_create['YEARS_IDE'] == '1-3').sum()],
    "4-6":[(df_to_create['YEARS_PRG'] == '4-6').sum(), (df_to_create['YEARS_IDE'] == '4-6').sum()],
    "7-10":[(df_to_create['YEARS_PRG'] == '7-10').sum(), (df_to_create['YEARS_IDE'] == '7-10').sum()],
    "10+":[(df_to_create['YEARS_PRG'] == '10+').sum(), (df_to_create['YEARS_IDE'] == '10+').sum()]
    }, 
    index=["Years PRG", "Years IDE"]
)

# Screen

df_to_create = screen_results

screen_explvl_df = pd.DataFrame({
    "<1":[(df_to_create['YEARS_PRG'] == '<1').sum(), (df_to_create['YEARS_IDE'] == '<1').sum()],
    "1-3":[(df_to_create['YEARS_PRG'] == '1-3').sum(), (df_to_create['YEARS_IDE'] == '1-3').sum()],
    "4-6":[(df_to_create['YEARS_PRG'] == '4-6').sum(), (df_to_create['YEARS_IDE'] == '4-6').sum()],
    "7-10":[(df_to_create['YEARS_PRG'] == '7-10').sum(), (df_to_create['YEARS_IDE'] == '7-10').sum()],
    "10+":[(df_to_create['YEARS_PRG'] == '10+').sum(), (df_to_create['YEARS_IDE'] == '10+').sum()]
    }, 
    index=["Years PRG", "Years IDE"]
)

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10, 4.5))
vr_explvl_df.plot(ax=axes[0], kind="bar", rot=0)
axes[0].legend(ncol=5,loc = 'upper right')
axes[0].set_ylabel("VR participant")
screen_explvl_df.plot(ax=axes[1], kind="bar", rot=0)
axes[1].legend(ncol=5,loc = 'upper right')
axes[1].set_ylabel("Screen participant")

# Uncomment to save fig
# plt.savefig('expyears2.png')
```




    Text(0, 0.5, 'Screen participant')




    
![png](output_4_1.png)
    


--------

## Task answers

In this section we analyze the tasks answer of the participants (both VR and Screen).

We have as well the right results (including the top 5 files)


```python
right_answers = pd.read_csv('../results/references.csv')
```


```python
# Task 1
task1_answers_counts = pd.DataFrame({ 'VR': vr_results['T1_location'].value_counts(), 'Screen': screen_results['T1_location'].value_counts()}).fillna(0).astype(int)
task1_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR</th>
      <th>Screen</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Left side</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>All inside the left part of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>All the neighborhood on the left</th>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>All the west side of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Big block west side of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Everything under the test/ folder</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Left</th>
      <td>8</td>
      <td>0</td>
    </tr>
    <tr>
      <th>Left side</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Left side, test district</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Left side, under the test folder</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Left under test/ca folder</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>On the left</th>
      <td>4</td>
      <td>0</td>
    </tr>
    <tr>
      <th>Test files west side</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>West side of the city</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>test block</th>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>test folder, left</th>
      <td>0</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 1 right answer
right_answers[right_answers['TASK ID'] == "T1_location"]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
      <th>METRIC VALUE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>T1_location</td>
      <td>test folder, west zone of the city</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 2
task2_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T2_file1'].value_counts(), 'VR file 2': vr_results['T2_file2'].value_counts(), 'VR file 3': vr_results['T2_file3'].value_counts(), 'Screen file 1': screen_results['T2_file1'].value_counts(), 'Screen file 2': screen_results['T2_file2'].value_counts(), 'Screen file 3': screen_results['T2_file3'].value_counts()}).fillna(0).astype(int)
task2_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>DiagramCanvasController.java</th>
      <td>0</td>
      <td>0</td>
      <td>3</td>
      <td>0</td>
      <td>1</td>
      <td>5</td>
    </tr>
    <tr>
      <th>DiagramTab.java</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>EditorFrame.java</th>
      <td>2</td>
      <td>11</td>
      <td>0</td>
      <td>1</td>
      <td>9</td>
      <td>0</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>11</td>
      <td>0</td>
      <td>0</td>
      <td>10</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>0</td>
      <td>2</td>
      <td>10</td>
      <td>2</td>
      <td>3</td>
      <td>7</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 2 percentage
def applyper(x):
    return (x/13)*100

task2_answers_counts.applymap(applyper).round(2).astype(str) + '%'
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>DiagramCanvasController.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>23.08%</td>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>38.46%</td>
    </tr>
    <tr>
      <th>DiagramTab.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>7.69%</td>
    </tr>
    <tr>
      <th>EditorFrame.java</th>
      <td>15.38%</td>
      <td>84.62%</td>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>69.23%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>84.62%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>76.92%</td>
      <td>0.0%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>0.0%</td>
      <td>15.38%</td>
      <td>76.92%</td>
      <td>15.38%</td>
      <td>23.08%</td>
      <td>53.85%</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 2 right answer
right_answers[right_answers['TASK ID'].isin(['T2_file1', 'T2_file2', 'T2_file3', 'T2_file4', 'T2_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
      <th>METRIC VALUE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>T2_file1</td>
      <td>JSONObject.java</td>
      <td>36.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>T2_file2</td>
      <td>EditorFrame.java</td>
      <td>34.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>T2_file3</td>
      <td>SegmentationStyleFactory.java</td>
      <td>33.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>T2_file4</td>
      <td>DiagramCanvasController.java</td>
      <td>32.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>T2_file5</td>
      <td>SelectionModel.java</td>
      <td>26.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 3
task3_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T3_file1'].value_counts(), 'VR file 2': vr_results['T3_file2'].value_counts(), 'VR file 3': vr_results['T3_file3'].value_counts(), 'Screen file 1': screen_results['T3_file1'].value_counts(), 'Screen file 2': screen_results['T3_file2'].value_counts(), 'Screen file 3': screen_results['T3_file3'].value_counts()}).fillna(0).astype(int)
task3_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ActorNodeViewer.java</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>ArrowHeadView.java</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>DiagramBuilder.java</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>EditorFrame.java</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>MoveTracker.java</th>
      <td>0</td>
      <td>11</td>
      <td>0</td>
      <td>0</td>
      <td>6</td>
      <td>0</td>
    </tr>
    <tr>
      <th>Node.java</th>
      <td>0</td>
      <td>1</td>
      <td>11</td>
      <td>0</td>
      <td>5</td>
      <td>8</td>
    </tr>
    <tr>
      <th>Prototypes.java</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>SequenceDiagramViewer.java</th>
      <td>12</td>
      <td>0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 3 percentage
def applyper(x):
    return (x/13)*100

task3_answers_counts.applymap(applyper).round(2).astype(str) + '%'
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ActorNodeViewer.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>7.69%</td>
    </tr>
    <tr>
      <th>ArrowHeadView.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>15.38%</td>
    </tr>
    <tr>
      <th>DiagramBuilder.java</th>
      <td>7.69%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>EditorFrame.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>MoveTracker.java</th>
      <td>0.0%</td>
      <td>84.62%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>46.15%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>Node.java</th>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>84.62%</td>
      <td>0.0%</td>
      <td>38.46%</td>
      <td>61.54%</td>
    </tr>
    <tr>
      <th>Prototypes.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>15.38%</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>SequenceDiagramViewer.java</th>
      <td>92.31%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>100.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 3 right answer
right_answers[right_answers['TASK ID'].isin(['T3_file1', 'T3_file2', 'T3_file3', 'T3_file4', 'T3_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
      <th>METRIC VALUE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6</th>
      <td>T3_file1</td>
      <td>SequenceDiagramViewer.java</td>
      <td>27.50</td>
    </tr>
    <tr>
      <th>7</th>
      <td>T3_file2</td>
      <td>MoveTracker.java</td>
      <td>25.50</td>
    </tr>
    <tr>
      <th>8</th>
      <td>T3_file3</td>
      <td>Node.java</td>
      <td>25.00</td>
    </tr>
    <tr>
      <th>9</th>
      <td>T3_file4</td>
      <td>Prototypes.java</td>
      <td>24.75</td>
    </tr>
    <tr>
      <th>10</th>
      <td>T3_file5</td>
      <td>ArrowHeadView.java</td>
      <td>24.67</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 4
task4_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T4_file1'].value_counts(), 'VR file 2': vr_results['T4_file2'].value_counts(), 'VR file 3': vr_results['T4_file3'].value_counts(), 'Screen file 1': screen_results['T4_file1'].value_counts(), 'Screen file 2': screen_results['T4_file2'].value_counts(), 'Screen file 3': screen_results['T4_file3'].value_counts()}).fillna(0).astype(int)
task4_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ControlFlow.java</th>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>DiagramBuilder.java</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>DiagramCanvasController.java</th>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>0</td>
      <td>2</td>
      <td>3</td>
    </tr>
    <tr>
      <th>EditorFrame.java</th>
      <td>3</td>
      <td>6</td>
      <td>3</td>
      <td>2</td>
      <td>2</td>
      <td>7</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>4</td>
      <td>0</td>
      <td>4</td>
      <td>6</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>6</td>
      <td>5</td>
      <td>2</td>
      <td>5</td>
      <td>7</td>
      <td>1</td>
    </tr>
    <tr>
      <th>TipDialog.java</th>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 4 percentage
def applyper(x):
    return (x/13)*100

task4_answers_counts.applymap(applyper).round(2).astype(str) + '%'
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ControlFlow.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>15.38%</td>
    </tr>
    <tr>
      <th>DiagramBuilder.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>DiagramCanvasController.java</th>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>23.08%</td>
      <td>0.0%</td>
      <td>15.38%</td>
      <td>23.08%</td>
    </tr>
    <tr>
      <th>EditorFrame.java</th>
      <td>23.08%</td>
      <td>46.15%</td>
      <td>23.08%</td>
      <td>15.38%</td>
      <td>15.38%</td>
      <td>53.85%</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>30.77%</td>
      <td>0.0%</td>
      <td>30.77%</td>
      <td>46.15%</td>
      <td>7.69%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>46.15%</td>
      <td>38.46%</td>
      <td>15.38%</td>
      <td>38.46%</td>
      <td>53.85%</td>
      <td>7.69%</td>
    </tr>
    <tr>
      <th>TipDialog.java</th>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 4 right answer
right_answers[right_answers['TASK ID'].isin(['T4_file1', 'T4_file2', 'T4_file3', 'T4_file4', 'T4_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
      <th>METRIC VALUE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>11</th>
      <td>T4_file1</td>
      <td>SegmentationStyleFactory.java</td>
      <td>570.0</td>
    </tr>
    <tr>
      <th>12</th>
      <td>T4_file2</td>
      <td>EditorFrame.java</td>
      <td>567.0</td>
    </tr>
    <tr>
      <th>13</th>
      <td>T4_file3</td>
      <td>JSONObject.java</td>
      <td>534.0</td>
    </tr>
    <tr>
      <th>14</th>
      <td>T4_file4</td>
      <td>DiagramCanvasController.java</td>
      <td>448.0</td>
    </tr>
    <tr>
      <th>15</th>
      <td>T4_file5</td>
      <td>StateTransitionEdgeViewer.java</td>
      <td>357.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5
task5_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T5_file1'].value_counts(), 'VR file 2': vr_results['T5_file2'].value_counts(), 'VR file 3': vr_results['T5_file3'].value_counts(), 'Screen file 1': screen_results['T5_file1'].value_counts(), 'Screen file 2': screen_results['T5_file2'].value_counts(), 'Screen file 3': screen_results['T5_file3'].value_counts()}).fillna(0).astype(int)
task5_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ControlFlow.java</th>
      <td>0</td>
      <td>0</td>
      <td>12</td>
      <td>0</td>
      <td>0</td>
      <td>9</td>
    </tr>
    <tr>
      <th>DiagramCanvasController.java</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>4</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>13</td>
      <td>0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>0</td>
      <td>13</td>
      <td>0</td>
      <td>0</td>
      <td>13</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5 percentage
def applyper(x):
    return (x/13)*100

task5_answers_counts.applymap(applyper).round(2).astype(str) + '%'
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>VR file 2</th>
      <th>VR file 3</th>
      <th>Screen file 1</th>
      <th>Screen file 2</th>
      <th>Screen file 3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ControlFlow.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>92.31%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>69.23%</td>
    </tr>
    <tr>
      <th>DiagramCanvasController.java</th>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>7.69%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>30.77%</td>
    </tr>
    <tr>
      <th>JSONObject.java</th>
      <td>100.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>100.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>SegmentationStyleFactory.java</th>
      <td>0.0%</td>
      <td>100.0%</td>
      <td>0.0%</td>
      <td>0.0%</td>
      <td>100.0%</td>
      <td>0.0%</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 5 right answer
right_answers[right_answers['TASK ID'].isin(['T5_file1', 'T5_file2', 'T5_file3', 'T5_file4', 'T5_file5'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
      <th>METRIC VALUE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>16</th>
      <td>T5_file1</td>
      <td>JSONObject.java</td>
      <td>195.0</td>
    </tr>
    <tr>
      <th>17</th>
      <td>T5_file2</td>
      <td>SegmentationStyleFactory.java</td>
      <td>120.0</td>
    </tr>
    <tr>
      <th>18</th>
      <td>T5_file3</td>
      <td>ControlFlow.java</td>
      <td>95.0</td>
    </tr>
    <tr>
      <th>19</th>
      <td>T5_file4</td>
      <td>DiagramCanvasController.java</td>
      <td>87.0</td>
    </tr>
    <tr>
      <th>20</th>
      <td>T5_file5</td>
      <td>JSONTokener.java</td>
      <td>65.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 6
task6_answers_counts = pd.DataFrame({ 'VR file 1': vr_results['T6_file1'].value_counts(), 'Screen file 1': screen_results['T6_file1'].value_counts()}).fillna(0).astype(int)
task6_answers_counts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>Screen file 1</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ActorNode.java</th>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>ImplicitParameterNode.java</th>
      <td>11</td>
      <td>8</td>
    </tr>
    <tr>
      <th>PackageNode.java</th>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>SequenceDiagramViewer.java</th>
      <td>0</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 6 percentage
def applyper(x):
    return (x/13)*100

task6_answers_counts.applymap(applyper).round(2).astype(str) + '%'
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>VR file 1</th>
      <th>Screen file 1</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ActorNode.java</th>
      <td>7.69%</td>
      <td>0.0%</td>
    </tr>
    <tr>
      <th>ImplicitParameterNode.java</th>
      <td>84.62%</td>
      <td>61.54%</td>
    </tr>
    <tr>
      <th>PackageNode.java</th>
      <td>7.69%</td>
      <td>30.77%</td>
    </tr>
    <tr>
      <th>SequenceDiagramViewer.java</th>
      <td>0.0%</td>
      <td>7.69%</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Task 6 right answer
right_answers[right_answers['TASK ID'].isin(['T6_file1', 'T6_file2', 'T6_file3'])]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TASK ID</th>
      <th>CORRECT ANSWER</th>
      <th>METRIC VALUE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>21</th>
      <td>T6_file1</td>
      <td>ImplicitParameterNode.java</td>
      <td>9.17</td>
    </tr>
    <tr>
      <th>22</th>
      <td>T6_file2</td>
      <td>PackageNode.java</td>
      <td>9.14</td>
    </tr>
    <tr>
      <th>23</th>
      <td>T6_file3</td>
      <td>ActorNode.java</td>
      <td>9.00</td>
    </tr>
  </tbody>
</table>
</div>



--------

## Task Difficulty

In this section we analyze the answers of the participants related to the difficulty of the tasks


```python
vr_dificult_counts = pd.DataFrame({ 'T1 dif': vr_results['T1_dif'].value_counts(), 'T2 dif': vr_results['T2_dif'].value_counts(), 'T3 dif': vr_results['T3_dif'].value_counts(), 'T4 dif': vr_results['T4_dif'].value_counts(), 'T5 dif': vr_results['T5_dif'].value_counts(), 'T6 dif': vr_results['T6_dif'].value_counts(), 'Overall': vr_results['DIFFICULT'].value_counts()}).fillna(0).astype(int)
# dn_row = pd.DataFrame({ 'T1 dif': 0, 'T2 dif': 0, 'T3 dif': 0, 'T4 dif': 0, 'T5 dif': 0, 'T6 dif': 0, 'Overall': 0 }, index = ["Don't know"])
# vr_dificult_counts = pd.concat([vr_dificult_counts, dn_row])
vr_dificult_counts
vr_dificult_counts = vr_dificult_counts.reindex(["Strongly Agree", "Agree", "Don't know", "Disagree", "Strongly Disagree"])

screen_dificult_counts = pd.DataFrame({ 'T1 dif': screen_results['T1_dif'].value_counts(), 'T2 dif': screen_results['T2_dif'].value_counts(), 'T3 dif': screen_results['T3_dif'].value_counts(), 'T4 dif': screen_results['T4_dif'].value_counts(), 'T5 dif': screen_results['T5_dif'].value_counts(), 'T6 dif': screen_results['T6_dif'].value_counts(), 'Overall': screen_results['DIFFICULT'].value_counts()}).fillna(0).astype(int)
screen_dificult_counts = screen_dificult_counts.reindex(["Strongly Agree", "Agree", "Don't know", "Disagree", "Strongly Disagree"])

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(10, 4.5))
vr_dificult_counts.transpose().plot(ax=axes[0], kind="bar", rot=0)
axes[0].legend(ncol=5,loc = 'upper right')
axes[0].set_ylabel("VR participant")
screen_dificult_counts.transpose().plot(ax=axes[1], kind="bar", rot=0)
axes[1].legend(ncol=5,loc = 'upper right')
axes[1].set_ylabel("Screen participant")

# Uncomment to save fig
# plt.savefig('subdif2.png')
```




    Text(0, 0.5, 'Screen participant')




    
![png](output_25_1.png)
    


--------

## Task Times

In this section we analyze the time that the participants needed to answer the questions


```python
# Get only the time columns
vr_results_times = vr_results[['T1_dt', 'T2_dt', 'T3_dt', 'T4_dt', 'T5_dt', 'T6_dt', 'T7_dt']]
screen_results_times = screen_results[['T1_dt', 'T2_dt', 'T3_dt', 'T4_dt', 'T5_dt', 'T6_dt', 'T7_dt']]

# Add time columns in seconds format in order to print boxplots
vr_results_times['T1 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T1_dt']]).total_seconds()
vr_results_times['T2 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T2_dt']]).total_seconds()
vr_results_times['T3 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T3_dt']]).total_seconds()
vr_results_times['T4 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T4_dt']]).total_seconds()
vr_results_times['T5 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T5_dt']]).total_seconds()
vr_results_times['T6 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T6_dt']]).total_seconds()
vr_results_times['T7 sec'] = pd.to_timedelta(['00:'+i for i in vr_results_times['T7_dt']]).total_seconds()

screen_results_times['T1 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T1_dt']]).total_seconds()
screen_results_times['T2 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T2_dt']]).total_seconds()
screen_results_times['T3 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T3_dt']]).total_seconds()
screen_results_times['T4 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T4_dt']]).total_seconds()
screen_results_times['T5 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T5_dt']]).total_seconds()
screen_results_times['T6 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T6_dt']]).total_seconds()
screen_results_times['T7 sec'] = pd.to_timedelta(['00:'+i for i in screen_results_times['T7_dt']]).total_seconds()
```


```python
# Boxplot for VR
fig = plt.figure(figsize=(7, 6))
plt.subplot(2, 1, 1)
plt.ylim(0,550)
vr_boxplot = vr_results_times.boxplot(column=['T1 sec', 'T2 sec', 'T3 sec', 'T4 sec', 'T5 sec', 'T6 sec', 'T7 sec'], rot=0)
plt.ylabel("VR participant")

# Boxplot for screen
plt.subplot(2, 1, 2)
plt.ylim(0,550)
screen_boxplot = screen_results_times.boxplot(column=['T1 sec', 'T2 sec', 'T3 sec', 'T4 sec', 'T5 sec', 'T6 sec', 'T7 sec'], rot=0)
plt.ylabel("Screen participant")

# Uncomment to save fig
# plt.savefig('boxplots_samefigure2.png')
```




    Text(0, 0.5, 'Screen participant')




    
![png](output_28_1.png)
    



```python
# Get Average table
screen_times_means = screen_results_times[['T1 sec', 'T2 sec', 'T3 sec', 'T4 sec', 'T5 sec', 'T6 sec', 'T7 sec']].mean()
vr_times_means = vr_results_times[['T1 sec', 'T2 sec', 'T3 sec', 'T4 sec', 'T5 sec', 'T6 sec', 'T7 sec']].mean()


table_avgs_raw = pd.DataFrame({'vr': vr_times_means, 'screen': screen_times_means, 'diff': screen_times_means - vr_times_means})
table_avgs_raw.rename(index={'T1 sec': 'T1 avg', 'T2 sec': 'T2 avg', 'T3 sec': 'T3 avg', 'T4 sec': 'T4 avg', 'T5 sec': 'T5 avg', 'T6 sec': 'T6 avg' ,'T7 sec': 'T7 avg'}, inplace=True)
# here is in seconds
table_avgs = table_avgs_raw.round().apply(pd.to_timedelta, unit='s')
table_avgs
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>vr</th>
      <th>screen</th>
      <th>diff</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>T1 avg</th>
      <td>0 days 00:00:47</td>
      <td>0 days 00:02:13</td>
      <td>0 days 00:01:26</td>
    </tr>
    <tr>
      <th>T2 avg</th>
      <td>0 days 00:01:24</td>
      <td>0 days 00:03:01</td>
      <td>0 days 00:01:37</td>
    </tr>
    <tr>
      <th>T3 avg</th>
      <td>0 days 00:01:37</td>
      <td>0 days 00:02:05</td>
      <td>0 days 00:00:28</td>
    </tr>
    <tr>
      <th>T4 avg</th>
      <td>0 days 00:01:35</td>
      <td>0 days 00:02:18</td>
      <td>0 days 00:00:43</td>
    </tr>
    <tr>
      <th>T5 avg</th>
      <td>0 days 00:00:41</td>
      <td>0 days 00:01:03</td>
      <td>0 days 00:00:23</td>
    </tr>
    <tr>
      <th>T6 avg</th>
      <td>0 days 00:01:49</td>
      <td>0 days 00:02:31</td>
      <td>0 days 00:00:42</td>
    </tr>
    <tr>
      <th>T7 avg</th>
      <td>0 days 00:01:58</td>
      <td>0 days 00:02:46</td>
      <td>0 days 00:00:48</td>
    </tr>
  </tbody>
</table>
</div>




```python
from string import Template

class DeltaTemplate(Template):
    delimiter = "%"
    
def strfdelta(tdelta, fmt):
    d = {}
    d["H"], rem = divmod(tdelta.seconds, 3600)
    d["M"], d["S"] = divmod(rem, 60)
    t = DeltaTemplate(fmt)
    return t.substitute(**d)

# Friendly table

table_avgs['vr'] = table_avgs['vr'].apply(lambda x: strfdelta(x, '%M:%S'))
table_avgs['screen'] = table_avgs['screen'].apply(lambda x: strfdelta(x, '%M:%S'))
table_avgs['diff'] = table_avgs['diff'].apply(lambda x: strfdelta(x, '%M:%S'))
table_avgs
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>vr</th>
      <th>screen</th>
      <th>diff</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>T1 avg</th>
      <td>0:47</td>
      <td>2:13</td>
      <td>1:26</td>
    </tr>
    <tr>
      <th>T2 avg</th>
      <td>1:24</td>
      <td>3:1</td>
      <td>1:37</td>
    </tr>
    <tr>
      <th>T3 avg</th>
      <td>1:37</td>
      <td>2:5</td>
      <td>0:28</td>
    </tr>
    <tr>
      <th>T4 avg</th>
      <td>1:35</td>
      <td>2:18</td>
      <td>0:43</td>
    </tr>
    <tr>
      <th>T5 avg</th>
      <td>0:41</td>
      <td>1:3</td>
      <td>0:23</td>
    </tr>
    <tr>
      <th>T6 avg</th>
      <td>1:49</td>
      <td>2:31</td>
      <td>0:42</td>
    </tr>
    <tr>
      <th>T7 avg</th>
      <td>1:58</td>
      <td>2:46</td>
      <td>0:48</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Total AVG

vr_secs_df = vr_results_times[['T1 sec', 'T2 sec', 'T3 sec', 'T4 sec', 'T5 sec', 'T6 sec', 'T7 sec']]
# vr_secs_df["Total"] = vr_secs_df.sum(axis=1)
total_vr = vr_secs_df.values.mean()
print("VR all avg: {}".format(strfdelta(pd.to_timedelta(total_vr, unit='s'), '%M:%S')))

screen_secs_df = screen_results_times[['T1 sec', 'T2 sec', 'T3 sec', 'T4 sec', 'T5 sec', 'T6 sec', 'T7 sec']]
# screen_secs_df["Total"] = screen_secs_df.sum(axis=1)
total_screen = screen_secs_df.values.mean()
print("Screen all avg: {}".format(strfdelta(pd.to_timedelta(total_screen, unit='s'), '%M:%S')))

```

    VR all avg: 1:24
    Screen all avg: 2:16



```python
from scipy.stats import mannwhitneyu

mannwhitney_df = pd.DataFrame(columns=["U", "p-value"])

# Task 1
stat, p = mannwhitneyu(screen_results_times['T1 sec'].values, vr_results_times['T1 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T1"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 2
stat, p = mannwhitneyu(screen_results_times['T2 sec'].values, vr_results_times['T2 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T2"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 3
stat, p = mannwhitneyu(screen_results_times['T3 sec'].values, vr_results_times['T3 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T3"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 4
stat, p = mannwhitneyu(screen_results_times['T4 sec'].values, vr_results_times['T4 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T4"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 5
stat, p = mannwhitneyu(screen_results_times['T5 sec'].values, vr_results_times['T5 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T5"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 6
stat, p = mannwhitneyu(screen_results_times['T6 sec'].values, vr_results_times['T6 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T6"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Task 6
stat, p = mannwhitneyu(screen_results_times['T7 sec'].values, vr_results_times['T7 sec'].values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["T7"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# All
screen_results_times_concat = pd.concat([screen_results_times['T1 sec'], screen_results_times['T2 sec'], screen_results_times['T3 sec'], screen_results_times['T4 sec'], screen_results_times['T5 sec'], screen_results_times['T6 sec'],screen_results_times['T7 sec']])
vr_results_times_concat = pd.concat([vr_results_times['T1 sec'], vr_results_times['T2 sec'], vr_results_times['T3 sec'], vr_results_times['T4 sec'], vr_results_times['T5 sec'], vr_results_times['T6 sec'],vr_results_times['T7 sec']])
stat, p = mannwhitneyu(screen_results_times_concat.values, vr_results_times_concat.values, alternative="two-sided")
dn_row = pd.DataFrame({ 'U': stat, 'p-value': p }, index = ["All"])
mannwhitney_df = pd.concat([mannwhitney_df, dn_row])

# Show table
mannwhitney_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>U</th>
      <th>p-value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>T1</th>
      <td>156.0</td>
      <td>2.715541e-04</td>
    </tr>
    <tr>
      <th>T2</th>
      <td>162.0</td>
      <td>7.857086e-05</td>
    </tr>
    <tr>
      <th>T3</th>
      <td>124.0</td>
      <td>4.546335e-02</td>
    </tr>
    <tr>
      <th>T4</th>
      <td>127.0</td>
      <td>3.125224e-02</td>
    </tr>
    <tr>
      <th>T5</th>
      <td>144.0</td>
      <td>2.476825e-03</td>
    </tr>
    <tr>
      <th>T6</th>
      <td>142.5</td>
      <td>3.128867e-03</td>
    </tr>
    <tr>
      <th>T7</th>
      <td>126.0</td>
      <td>3.547245e-02</td>
    </tr>
    <tr>
      <th>All</th>
      <td>6429.5</td>
      <td>1.193074e-10</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Cliff Effect Size

from __future__ import division

def cliffsDelta(lst1,lst2,
                dull = [0.147, # small
                        0.33,  # medium
                        0.474 # large
                        ][0] ): 
  "Returns true if there are more than 'dull' differences"
  m, n = len(lst1), len(lst2)
  lst2 = sorted(lst2)
  j = more = less = 0
  for repeats,x in runs(sorted(lst1)):
    while j <= (n - 1) and lst2[j] <  x: 
      j += 1
    more += j*repeats
    while j <= (n - 1) and lst2[j] == x: 
      j += 1
    less += (n - j)*repeats
  d= (more - less) / (m*n) 
  return abs(d), abs(d)  > dull
  
def runs(lst):
  "Iterator, chunks repeated values"
  for j,two in enumerate(lst):
    if j == 0:
      one,i = two,0
    if one!=two:
      yield j - i,one
      i = j
    one=two
  yield j - i + 1,two

# Cliff Effect Size Dataframe
cliff_df = pd.DataFrame(columns=["Cliff Eff Size"])

# Task 1
stat, isdif = cliffsDelta(screen_results_times['T1 sec'].values, vr_results_times['T1 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T1"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 2
stat, isdif = cliffsDelta(screen_results_times['T2 sec'].values, vr_results_times['T2 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T2"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 3
stat, isdif = cliffsDelta(screen_results_times['T3 sec'].values, vr_results_times['T2 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T3"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 4
stat, isdif = cliffsDelta(screen_results_times['T4 sec'].values, vr_results_times['T4 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T4"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 5
stat, isdif = cliffsDelta(screen_results_times['T5 sec'].values, vr_results_times['T5 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T5"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 6
stat, isdif = cliffsDelta(screen_results_times['T6 sec'].values, vr_results_times['T6 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T6"])
cliff_df = pd.concat([cliff_df, dn_row])

# Task 7
stat, isdif = cliffsDelta(screen_results_times['T7 sec'].values, vr_results_times['T7 sec'].values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["T7"])
cliff_df = pd.concat([cliff_df, dn_row])

# All
stat, isdif = cliffsDelta(screen_results_times_concat.values, vr_results_times_concat.values)
dn_row = pd.DataFrame({ 'Cliff Eff Size': stat }, index = ["All"])
cliff_df = pd.concat([cliff_df, dn_row])

# Show table
cliff_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Cliff Eff Size</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>T1</th>
      <td>0.846154</td>
    </tr>
    <tr>
      <th>T2</th>
      <td>0.917160</td>
    </tr>
    <tr>
      <th>T3</th>
      <td>0.639053</td>
    </tr>
    <tr>
      <th>T4</th>
      <td>0.502959</td>
    </tr>
    <tr>
      <th>T5</th>
      <td>0.704142</td>
    </tr>
    <tr>
      <th>T6</th>
      <td>0.686391</td>
    </tr>
    <tr>
      <th>T7</th>
      <td>0.491124</td>
    </tr>
    <tr>
      <th>All</th>
      <td>0.552832</td>
    </tr>
  </tbody>
</table>
</div>


